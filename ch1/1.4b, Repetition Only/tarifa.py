import sys

x = int(sys.stdin.readline())
n = int(sys.stdin.readline())
lines = map(int, sys.stdin.readlines())
cum = 0
for p in lines:
    cum += x - p
print(cum + x)
