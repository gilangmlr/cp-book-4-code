import sys

attacks = sys.stdin.readline().strip()

counter = {
    'R': 'S',
    'B': 'K',
    'L': 'H'
}

counters = []
i = 0
while i < len(attacks):
    if i + 2 < len(attacks) and len(set(attacks[i:i+3])) == 3:
        counters.append('C')
        i += 3
    else:
        counters.append(counter[attacks[i]])
        i += 1

print(''.join(counters))
