from collections import defaultdict
import sys

c = int(sys.stdin.readline())
for case in range(c):
    s = sys.stdin.readline().strip()
    t = sys.stdin.readline().strip()

    freq = defaultdict(int)
    for i, c in enumerate(s):
        if c == '?':
            freq['?'] += 1
        elif c != t[i]:
            freq[c] += 1
        if c == '1':
            freq['s1'] += 1
        if t[i] == '1':
            freq['t1'] += 1

    if freq['s1'] > freq['t1']:
        print(f'Case {case + 1}: {-1}')
    else:
        moves = 0
        if freq['1'] <= freq['0']:
            moves += freq['0'] + freq['?']
        else:
            moves += freq['0']
            freq['1'] -= freq['0']
            freq['0'] = 0

            # change ? to 0 which will be swapped with 1
            moves += freq['1'] * 2
            freq['?'] -= freq['1']
            freq['1'] = 0

            # change ? to 0 or 1
            moves += freq['?']

        print(f'Case {case + 1}: {moves}')
