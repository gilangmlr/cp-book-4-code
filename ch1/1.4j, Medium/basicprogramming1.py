import sys

rl = sys.stdin.readline

[n, t] = map(int, rl().split(' '))
nums = list(map(int, rl().split(' ')))

if t == 1:
    print(7)
elif t == 2:
    if nums[0] > nums[1]:
        print('Bigger')
    elif nums[0] == nums[1]:
        print('Equal')
    else:
        print('Smaller')
elif t == 3:
    print(sorted(nums[:3])[1])
elif t == 4:
    print(sum(nums))
elif t == 5:
    print(sum(filter(lambda num: num % 2 == 0, nums)))
elif t == 6:
    chars = map(lambda num: chr(ord('a') + (num % 26)), nums)
    print(''.join(chars))
elif t == 7:
    visited = set()
    i = 0
    while True:
        if nums[i] < 0 or nums[i] >= n:
            print('Out')
            break
        elif nums[i] == n - 1:
            print('Done')
            break
        else:
            if nums[i] in visited:
                print('Cyclic')
                break
            visited.add(nums[i])
            i = nums[i]
