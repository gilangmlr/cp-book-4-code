import sys

t = int(sys.stdin.readline())


def is_eligible(st, stickers):
    eligible = True
    for s in st:
        if stickers[s - 1] < 1:
            eligible = False
    return eligible


def claim_prize(st, stickers):
    for s in st:
        stickers[s - 1] -= 1


for _ in range(t):
    [n, m] = map(int, sys.stdin.readline().split(' '))
    prizes = []
    for _ in range(n):
        [k, *st, p] = map(int, sys.stdin.readline().split(' '))
        prizes.append((st, p))
    stickers = list(map(int, sys.stdin.readline().split(' ')))

    total_prize = 0
    for st, p in prizes:
        while is_eligible(st, stickers):
            total_prize += p
            claim_prize(st, stickers)
    print(f'{total_prize}')
