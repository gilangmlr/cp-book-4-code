import datetime
import sys

tc = int(sys.stdin.readline())

while tc > 0:
    [name, postSecondary, dateOfBirth, courses] = sys.stdin.readline().split(' ')
    psYear = datetime.datetime.strptime(postSecondary, '%Y/%m/%d').year
    dobYear = datetime.datetime.strptime(dateOfBirth, '%Y/%m/%d').year
    courses = int(courses)
    eligibilityStatus = ''
    if psYear >= 2010 or dobYear >= 1991:
        eligibilityStatus = 'eligible'
    elif courses > 40:
        eligibilityStatus = 'ineligible'
    else:
        eligibilityStatus = 'coach petitions'
    print(f'{name} {eligibilityStatus}')

    tc -= 1
