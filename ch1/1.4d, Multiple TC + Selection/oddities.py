import sys

n = int(sys.stdin.readline())

while n > 0:
    x = int(sys.stdin.readline())
    if x % 2 == 0:
        print(f'{x} is even')
    else:
        print(f'{x} is odd')

    n -= 1
