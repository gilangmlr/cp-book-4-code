import sys

tc = int(sys.stdin.readline())

while tc > 0:
    op = sys.stdin.readline().strip()
    if op == 'P=NP':
        print('skipped')
    else:
        [a, b] = map(int, op.split('+'))
        print(f'{a+b}')

    tc -= 1
