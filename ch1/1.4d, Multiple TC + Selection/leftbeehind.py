import sys

while True:
    [sweet, sour] = map(int, sys.stdin.readline().split(' '))

    if sweet == 0 and sour == 0:
        break

    if sweet + sour == 13:
        print('Never speak again.')
    elif sweet < sour:
        print('Left beehind.')
    elif sweet == sour:
        print('Undecided.')
    elif sweet > sour:
        print('To the convention.')
