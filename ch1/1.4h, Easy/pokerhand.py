from collections import defaultdict
import sys

cards = sys.stdin.readline().split(' ')
freq = defaultdict(int)
strength = 0
for card in cards:
    freq[card[0]] += 1
    strength = max(strength, freq[card[0]])
print(strength)
