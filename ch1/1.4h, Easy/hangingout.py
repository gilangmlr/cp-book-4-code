import sys

[cap, x] = map(int, sys.stdin.readline().split(' '))
currCap = 0
totalDenied = 0
for _ in range(x):
    [e, p] = sys.stdin.readline().strip().split(' ')
    p = int(p)

    if e == 'leave':
        currCap -= p
    elif e == 'enter':
        if currCap + p <= cap:
            currCap += p
        else:
            totalDenied += 1
print(totalDenied)
