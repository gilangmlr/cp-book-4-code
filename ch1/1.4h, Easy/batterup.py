from statistics import mean
import sys

n = int(sys.stdin.readline())
nums = filter(lambda num: num >= 0, map(int, sys.stdin.readline().split(' ')))

print(mean(nums))
