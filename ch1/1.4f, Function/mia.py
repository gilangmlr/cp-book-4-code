import sys


def score(a, b):
    if (a == 1 and b == 2) or (a == 2 and b == 1):
        return (66 * 2) + 1
    elif a == b:
        return 66 + ((a * 10) + b)
    else:
        return (max(a, b) * 10) + min(a, b)


lines = sys.stdin.readlines()

for line in lines:
    [s0, s1, r0, r1] = map(int, line.split(' '))

    if s0 == 0 and s1 == 0 and r0 == 0 and r1 == 0:
        break

    score1 = score(s0, s1)
    score2 = score(r0, r1)

    if score1 > score2:
        print('Player 1 wins.')
    elif score1 < score2:
        print('Player 2 wins.')
    else:
        print('Tie.')
