import sys

lines = sys.stdin.readlines()

for line in lines:
    line = line.strip()
    if line == 'END':
        break
    xPrev = line
    x = str(len(xPrev))
    i = 1
    while xPrev != x:
        xPrev = x
        x = str(len(xPrev))
        i += 1
    print(i)
