from math import sin, cos
import sys

[p, a, b, c, d, n] = map(int, sys.stdin.readline().split(' '))


def price(k):
    return p * (sin(a * k + b) + cos(c * k + d) + 2)


maxLoss = 0
currMax = price(1)
for k in range(2, n + 1):
    pr = price(k)
    currMax = max(currMax, pr)
    maxLoss = max(maxLoss, currMax - pr)

print(maxLoss)
