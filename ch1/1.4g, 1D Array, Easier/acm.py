from collections import defaultdict
import sys

problemPenalties = defaultdict(int)
lines = sys.stdin.readlines()
solved = 0
timeScore = 0
for line in lines:
    line = line.strip()
    if line == '-1':
        break

    [m, p, v] = line.split(' ')
    m = int(m)

    if v == 'right':
        solved += 1
        timeScore += m + problemPenalties[p]
    elif v == 'wrong':
        problemPenalties[p] += 20

print(f'{solved} {timeScore}')
