import sys

nums = list(map(int, sys.stdin.readline().split(' ')))
[a, b, c] = sorted(nums)

diff1 = b - a
diff2 = c - b

if diff1 == diff2:
    print(c + diff2)
elif diff1 < diff2:
    print(b + diff1)
else:
    print(a + diff2)
