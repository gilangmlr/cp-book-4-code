import sys

n = int(sys.stdin.readline())

if n > 1:
    nums = list(sys.stdin.readline().split(' '))
    nums = [(int(num), idx) for idx, num in enumerate(nums)]
    nums.sort()

    print(1, end=' ')
    print(' '.join(map(lambda num: str(num[1] + 2), nums)))
else:
    print(1)
