import sys

n = int(sys.stdin.readline())
names = list(map(str.strip, sys.stdin.readlines()))

ordering = 0
for i in range(1, n):
    if names[i] > names[i - 1]:
        ordering += 1
    elif names[i] < names[i - 1]:
        ordering -= 1

if ordering == n - 1:
    print('INCREASING')
elif ordering == -(n - 1):
    print('DECREASING')
else:
    print('NEITHER')
