import sys

[a, b, c, n] = map(int, sys.stdin.readline().split(' '))

if a >= 1 and b >= 1 and c >= 1 and n >= 3 and n <= a + b + c:
    print('YES')
else:
    print('NO')
