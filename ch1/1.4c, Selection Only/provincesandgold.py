import sys

[g, s, c] = map(int, sys.stdin.readline().split(' '))

buyingPower = g * 3 + s * 2 + c * 1

cards = []
if buyingPower >= 8:
    cards.append('Province')
elif buyingPower >= 5:
    cards.append('Duchy')
elif buyingPower >= 2:
    cards.append('Estate')

if buyingPower >= 6:
    cards.append('Gold')
elif buyingPower >= 3:
    cards.append('Silver')
elif buyingPower >= 0:
    cards.append('Copper')

print(' or '.join(cards))
