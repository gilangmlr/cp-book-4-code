import sys

[month, date] = sys.stdin.readline().split(' ')

if (month == 'OCT' and int(date) == 31) or (month == 'DEC' and int(date) == 25):
    print('yup')
else:
    print('nope')
