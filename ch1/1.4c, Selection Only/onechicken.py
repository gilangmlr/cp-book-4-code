import sys

[n, m] = map(int, sys.stdin.readline().split(' '))
diff = m - n


def pieces(num):
    if num <= 1:
        return 'piece'
    else:
        return 'pieces'


if diff < 0:
    print(f'Dr. Chaz needs {abs(diff)} more {pieces(abs(diff))} of chicken!')
else:
    print(f'Dr. Chaz will have {diff} {pieces(diff)} of chicken left over!')
