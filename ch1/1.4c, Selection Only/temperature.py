import sys

# B = A + x
# A = yB

# B = yB + x
# B = x / (1 - y)

[x, y] = map(int, sys.stdin.readline().split(' '))
if x == 0 and y == 1:
    print('ALL GOOD')
elif y == 1:
    print('IMPOSSIBLE')
else:
    print(x / (1 - y))
