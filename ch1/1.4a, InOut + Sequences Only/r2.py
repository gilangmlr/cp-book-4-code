import sys

inputs = iter(sys.stdin.readlines())
r1, s = map(int, next(inputs).split())
print(str(2*s - r1))
