import sys

inputs = iter(sys.stdin.readlines())
n, p = map(int, next(inputs).split())
print(p)
