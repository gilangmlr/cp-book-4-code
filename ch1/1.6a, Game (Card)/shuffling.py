from math import ceil, floor
import sys

[n, dir] = sys.stdin.readline().strip().split(' ')

cards = [i for i in range(int(n))]


def is_original(cards):
    for i, c in enumerate(cards):
        if i != c:
            return False
    return True


def shuffle(cards, dir='out'):
    res = []
    if dir == 'out':
        mid = ceil(len(cards) / 2)
        last = cards[mid - 1]
        l = cards[:mid]
        r = cards[mid:]
    else:
        mid = floor(len(cards) / 2)
        last = cards[-1]
        r = cards[:mid]
        l = cards[mid:]

    for i, _ in enumerate(r):
        res.append(l[i])
        res.append(r[i])
    if len(cards) % 2 == 1:
        res.append(last)
        
    return res


shuffled = 0
curr = cards
while True:
    curr = shuffle(curr, dir)
    shuffled += 1
    if is_original(curr):
        break
print(shuffled)
