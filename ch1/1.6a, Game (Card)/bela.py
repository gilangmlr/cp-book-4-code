import sys

[n, s] = sys.stdin.readline().strip().split(' ')

scores = {
    'dominant': {'A': 11, 'K': 4, 'Q': 3, 'J': 20,
                 'T': 10, '9': 14, '8': 0, '7': 0},
    'non-dominant': {'A': 11, 'K': 4, 'Q': 3, 'J': 2,
                     'T': 10, '9': 0, '8': 0, '7': 0}
}

lines = sys.stdin.readlines()
score = 0
for line in lines:
    if line[1] == s:
        score += scores['dominant'][line[0]]
    else:
        score += scores['non-dominant'][line[0]]
print(score)
