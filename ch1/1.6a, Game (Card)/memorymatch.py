from collections import defaultdict
import re
import sys

p = re.compile(r'(\d+) (\d+) ([a-z]+) ([a-z]+)')

n = int(sys.stdin.readline())
k = int(sys.stdin.readline())

seen = defaultdict(set)
chosen = set()
flipped = set()
for _ in range(k):
    m = p.match(sys.stdin.readline())
    c1, c2, p1, p2 = m.groups()
    flipped.add(c1)
    flipped.add(c2)
    seen[p1].add(c1)
    seen[p2].add(c2)

    if p1 == p2:
        chosen.add(p1)
        del seen[p1]

pairs = 0
items = list(seen.items())
for key, value in items:
    if len(value) == 2:
        pairs += 1
        del seen[key]

if len(seen) == n - len(flipped):
    pairs += len(seen)
elif len(flipped) == n - 2:
    pairs += 1

print(pairs)
