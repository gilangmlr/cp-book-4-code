from math import inf
import sys

n = int(sys.stdin.readline())
tea_prices = list(map(int, sys.stdin.readline().split(' ')))
m = int(sys.stdin.readline())
topping_prices = list(map(int, sys.stdin.readline().split(' ')))

cheapest_tea_topping = inf
for i in range(n):
    [k, *toppings] = list(map(int, sys.stdin.readline().split(' ')))
    for topping_idx in toppings:
        cheapest_tea_topping = min(
            cheapest_tea_topping, tea_prices[i] + topping_prices[topping_idx - 1])

x = int(sys.stdin.readline())

print(max((x // cheapest_tea_topping) - 1, 0))
