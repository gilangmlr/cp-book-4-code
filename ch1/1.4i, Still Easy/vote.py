import sys

rl = sys.stdin.readline

t = int(rl())
for _ in range(t):
    n = int(rl())
    max_vote = -1
    winner = 0
    tie = False
    total_vote = 0
    for i in range(n):
        vote = int(rl())
        total_vote += vote
        if vote > max_vote:
            max_vote = vote
            winner = i + 1
            tie = False
        elif vote == max_vote:
            tie = True
    if tie:
        print('no winner')
    else:
        status = 'majority' if max_vote > total_vote / 2 else 'minority'
        print(f'{status} winner {winner}')
