import sys

n = int(sys.stdin.readline())
print(n - 2 if n > 3 else 1)
