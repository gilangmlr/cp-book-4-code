import sys

rl = sys.stdin.readline

n = int(rl())

found = False
for _ in range(n):
    k = int(rl())
    name = rl().strip()
    pea_exist = False
    pan_exist = False
    for _ in range(k):
        menu = rl().strip()
        if menu == 'pea soup':
            pea_exist = True
        elif menu == 'pancakes':
            pan_exist = True
    if pea_exist and pan_exist:
        print(name)
        found = True
        break

if not found:
    print('Anywhere is fine I guess')
