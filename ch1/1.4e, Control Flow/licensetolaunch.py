import sys

n = int(sys.stdin.readline())
junks = list(map(int, sys.stdin.readline().split(' ')))

print(junks.index(min(junks)))
