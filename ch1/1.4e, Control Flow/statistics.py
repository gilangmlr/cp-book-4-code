import sys

lines = sys.stdin.readlines()

for i, line in enumerate(lines):
    [n, *nums] = map(int, line.split(' '))
    minVal = min(nums)
    maxVal = max(nums)
    rangeVal = maxVal - minVal
    print(f'Case {i + 1}: {minVal} {maxVal} {rangeVal}')
