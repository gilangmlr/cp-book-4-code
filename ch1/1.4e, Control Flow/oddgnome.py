import sys

n = int(sys.stdin.readline())

for i in range(n):
    [g, *gnomes] = map(int, sys.stdin.readline().split(' '))
    for j in range(1, g):
        if gnomes[j] - 1 != gnomes[j - 1]:
            print(j + 1)
            break
